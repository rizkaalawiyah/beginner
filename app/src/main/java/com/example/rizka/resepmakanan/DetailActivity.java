package com.example.rizka.resepmakanan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailActivity extends AppCompatActivity {
    private ImageView foto;
    private TextView nama, deskripsi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        foto = findViewById(R.id.foto);
        nama = findViewById(R.id.tvNamaMakanan);
        deskripsi = findViewById(R.id.deskripsi);


        Intent i = getIntent();
        String imgfoto = i.getStringExtra("foto");
        String tvnama = i.getStringExtra("nama");
        String tvdeskripsi = i.getStringExtra("deskripsi");


        Glide.with(this).load(imgfoto).into(foto);

        nama.setText(tvnama);
        deskripsi.setText(tvdeskripsi);

    }
}

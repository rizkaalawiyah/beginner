package com.example.rizka.resepmakanan;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class MakananAdapter extends RecyclerView.Adapter<MakananAdapter.MyViewHolder> {
    private List<Makanan> listMakanan;
    private Context context;



    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imgPhoto;
        TextView tvName, tvRemarks;
        Button btnDetail;

        public MyViewHolder(View itemView){
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_item_photo);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvRemarks = itemView.findViewById(R.id.tv_item_remarks);
            btnDetail = itemView.findViewById(R.id.btn_set_detail);
        }
    }

    public MakananAdapter(List<Makanan> listMakanan, Context context) {
        this.listMakanan = listMakanan;
        this.context = context;
    }

    @Override
    public MakananAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_makanan, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Makanan makanan = listMakanan.get(position);

        Glide.with(context)
                .load(makanan.getPhoto())
                .override(350, 550)
                .into(holder.imgPhoto);


        holder.tvName.setText(makanan.getName());
        holder.tvRemarks.setText(makanan.getRemarks());
        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("foto", makanan.getPhoto());
                intent.putExtra("nama", makanan.getName());
                intent.putExtra("deskripsi", makanan.getDeskripsi());
                context.startActivity(new Intent(intent));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMakanan.size();
    }
}
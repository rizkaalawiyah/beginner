package com.example.rizka.resepmakanan;

import java.util.ArrayList;

public class MakananData {
    public static String[][] data = new String[][]{
            {"Mendoan", "Gorengan", "Mendoan tempe disajikan dalam keadaan panas disertai dengan cabe rawit atau sambal kecap. " +
                    "Mendoan tempe dapat dijadikan sebagai lauk makan ataupun makanan ringan untuk menemani minum teh " +
                    "atau kopi saat santai.", "https://img-global.cpcdn.com/003_recipes/12922fdbdae15bcf/640x640sq70/photo.jpg"},
            {"Bakwan", "Gorengan", "Bakwan adalah makanan gorengan yang terbuat dari sayuran dan tepung terigu yang lazim ditemukan di Indonesia." +
                    " Bakwan biasanya merujuk kepada kudapan gorengan sayur-sayuran yang biasa dijual oleh penjaja keliling gorengan.",
                    "https://img-global.cpcdn.com/003_recipes/774571d94019488e/640x640sq70/photo.jpg"},
            {"Gehu", "Gorengan","Gehu (singkatan dari toge tahu) adalah camilan khas Sunda yang terbuat dari tahu yang diisi dengan tauge dan kadang dicampur racikan bumbu pedas " +
                    "kemudian digoreng dengan pelapis yang terbuat dari adonan tepung terigu, tepung beras dan tepung kanji.", "https://img-global.cpcdn.com/003_recipes/4e7d57a8f7d7e425/640x640sq70/photo.jpg"},
            {"Pisang Goreng", "Gorengan", "Goreng pisang siap saji. Pisang goreng adalah makanan ringan yang banyak ditemui di Indonesia, Singapura dan Malaysia.", "https://img-global.cpcdn.com/003_recipes/c034dd5c328d5b99/640x640sq70/photo.jpg"},
            {"Cilung", "Jajanan", "\n" +
                    "Cilung adalah salah satu jajanan kuliner khas Bandung yang memang banyak digemari berbagai kalangan khususnya kalangan anak-anak. ", "https://img-global.cpcdn.com/003_recipes/77520ed6ab394057/640x640sq70/photo.jpg"},
            {"Cilok", "Jajanan", "Cilok adalah sebuah makanan khas Jawa Barat yang terbuat dari tapioka yang kenyal dengan tambahan bumbu pelengkap seperti sambal kacang, kecap, dan saus", "https://img-global.cpcdn.com/003_recipes/36a003866ab11b71/640x640sq70/photo.jpg"},
            {"Cimol", "Jajanan", "Cimol adalah makanan ringan yang dibuat dari tepung kanji. Cimol berasal dari kata (Bahasa Sunda aci digemol), yang artinya tepung kanji dibuat bulat-bulat.", "https://img-global.cpcdn.com/003_recipes/9694eb77ed99a6e1/640x640sq70/photo.jpg"}
    };



    public static ArrayList<Makanan> getListData(){
        Makanan makanan = null;
        ArrayList<Makanan> list = new ArrayList<>();
        for (int i = 0; i <data.length; i++) {
            makanan = new Makanan();
            makanan.setName(data[i][0]);
            makanan.setRemarks(data[i][1]);
            makanan.setDeskripsi(data[i][2]);
            makanan.setPhoto(data[i][3]);

            list.add(makanan);
        }

        return list;
    }
}

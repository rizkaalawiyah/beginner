package com.example.rizka.resepmakanan;

public class Makanan {
    private String name, remarks, photo, deskripsi;

    public Makanan() {
    }

    public Makanan(String name, String remarks, String photo, String deskripsi) {
        this.name = name;
        this.remarks = remarks;
        this.photo = photo;
        this.deskripsi = deskripsi;
    }

    //setter getter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }


}
